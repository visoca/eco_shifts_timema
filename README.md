# eco_shifts_timema

This repository contains a compilation of scripts used for 
processing and analysing data in: Muschick M, Soria-Carrasco V, Feder JL, 
Gompert Z, and Nosil P (2020). Adaptive zones shape the magnitude of premating
reproductive isolation in Timema stick insects. Philos. Trans. R. Soc. B
http://dx.doi.org/10.1098/rstb.2019.0541

Additional information can be found in the Electronic Supplementary Materials.
Data has been deposited in Dryad repository https://doi.org/10.5061/dryad.41ns1rnb6


All code tested on:

* Scientific Linux 6.1
* R 3.4.4
* Perl 5.26.1

Directories
===============================================================================
example
-------------------------------------------------------------------------------
Reduced datasets to test scripts. 

`alignments`: folder containing linkage-group example multiple alignments of
 variable positions
 
`res_jointML`: folder containing output files of transition analyses for 4
               example trees

`res_marginalML`:

`100kmreads_10kdepth_20QS_85cov_phylob.4runs.summary-ca.tree`:  Bayesian
 summary treee from Riesch et al. (2017) NEE
 
`phylohypos.trees`: example of phylogenetic hypotheses used with IQTREE and consel

transrate
-------------------------------------------------------------------------------


Scripts
===============================================================================

ancestral.besttree.R
-------------------------------------------------------------------------------
This script loads a tree and carry out transition rate analyses using 
hardcoded sates (i.e. hosts) for each tip.

Five different root probabilities are used:
flat, "yang", "maddfitz", con (only conifers), flo (only flowering plants)

Requirements: R>=3.4.4, corHMM>=1.24, diversitree

Use:
Rscript ancestral.besttree.R


autest.sh
-------------------------------------------------------------------------------
This script calculates AU tests given a set of alignments and phylogenetic
hypotheses. It depends on iqtree >=1.6.2, and two auxiliary custom perl scripts: 
concatenate.pl and  aliconverter.pl. The script needs to be edited to set up
the path to IQTREE and the auxiliary scripts, as well as the file with the 
phylogenetic hypotheses and the folder containing the alignments.

Use: `./autesth.sh`

concatenate.pl
-------------------------------------------------------------------------------
This auxiliary script (called from autest.sh) concatenates a set of mulitple 
alignments in fasta format and generates a text file with partition information.

Use: `./concatenate.pl -i alignment1.fa -i alignment2.fa -o alignment_out.fa -p`

aliconverter.pl
-------------------------------------------------------------------------------
This auxiliary script (called from autest.sh) converts among mulitple
alignments formats.

Use: `./aliconverter.pl -i alignment.fa -o alignment.phy -d dna`

dist2table.R
-------------------------------------------------------------------------------
This is an auxiliary R function called from `hpd.glm.R` that calculate a 
matrix of distances among groups.

hp.phylosig.R
-------------------------------------------------------------------------------
This script uses phylogenetic and host preference data to assess phylogenetic
conservatism by calculating Blomberg's K. This script needs to be edited to
set up working directory, input tree in nexus format, and host preference data.
Use: `Rscript hp.phylosig.R`

hpd.glm.R
-------------------------------------------------------------------------------
This script performs a beta regression to model the influence of divergence
in host plant use on divergence in host plant preference. The script needs to
be edited to set up the working directory, the path to the auxiliary function
`dist2table.R`, and files with information about host preference including 
locality coordinates, host plant divergence times, and host plant taxonomy 
(families). These files can be found in the
associated Dryad repository.

Use: `Rscript hpd.glm.R`

transition.phyluncert.par.jointML.sh
-------------------------------------------------------------------------------
This a shell script to run transition rate analyses estimating rates and states
jointly (using `transition.phyluncert.par.R`) in the Iceberg cluster of the 
University of Sheffield, which uses a SGE scheduler. 

transition.phyluncert.par.marginalML.sh
-------------------------------------------------------------------------------
This a shell script to run transition rate analyses estimating rates followed
by states (using `transition.phyluncert.par.R`) in the Iceberg cluster of the 
University of Sheffield, which uses a SGE scheduler.

transition.phyluncert.par.R
-------------------------------------------------------------------------------
This script loads a tree and carries out transition rate analyses using 
harcoded states (i.e. hosts) for each tip. It fits five different models using
five different root probabilities.

Use: `Rscript -t tree.nex -o outdir -method joint -recons estimate`

transition.phyluncert.summarize.jointML.R
-------------------------------------------------------------------------------
This script summarizes all the results of the joint estimation of rates and
states (script `transition.phyluncert.par.jointML.sh`) and save them in a single
RData file. The script needs to be edited to specify the input directory and 
the output RData file.

Use: `Rscript transition.phyluncert.summarize.jointML.R`

transition.phyluncert.summarize.marginalML.R
-------------------------------------------------------------------------------
This script summarizes all the results of the martginal estimation of rates and
states (script `transition.phyluncert.par.marginalML.sh`) and save them in a single
RData file. The script needs to be edited to specify the input directory and 
the output RData file.

Use: `Rscript transition.phyluncert.summarize.marginalML.R`

