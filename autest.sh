#!/bin/bash
#$ -l h_rt=300:00:00
#$ -P popgenom
#$ -q popgenom.q
#$ -pe openmp 24
#$ -j y
#$ -o autest.log

# request 24 threads, because asc and normal inferences
# run in parallel using 12 threads each

# programs and scripts
IQTREE="/usr/local/extras/Genomics/apps/iqtree/1.6.2/bin/iqtree"
CONCATENATE="/usr/local/extras/Genomics/scripts/phylogeny/concatenate.pl"
ALICONVERTER="/usr/local/extras/Genomics/scripts/phylogeny/aliconverter.pl"

PHYLOHYPOS="phylohypos.trees"
ALIDIR="alignments"
ALIS=$(ls $ALIDIR/*[0-9].fa | sort -V)

NTHREADS=12

# concatenate
# .............................................................................
CONCATCMD=$CONCATENATE
CONCATCMDVAR=$CONCATENATE
for ali in $ALIS;
do
	CONCATCMD="$CONCATCMD -i $ali"

	# get strictly variable sites using IQTREE (necessary to run ASC models)
	$IQTREE -nt $NTHREADS -st DNA -s $ali -m GTR+ASC >& /dev/null

	if [[ -f $ali.varsites.phy ]];
	then
		# clean files
		rm $ali.ckp.gz $ali.log
		# convert phy to fa
		$ALICONVERTER -i $ali.varsites.phy -o $ali.varsites.fa -d dna >& /dev/null
		CONCATCMDVAR="$CONCATCMDVAR -i $ali.varsites.fa"
	else
		CONCATCMDVAR="$CONCATCMDVAR -i $ali"
	fi
done
CONCATCMD="$CONCATCMD -p -o concatenated_lgs.fa"
CONCATCMDVAR="$CONCATCMDVAR -p -o concatenated_lgs.varsites.fa"

$CONCATCMD

$CONCATCMDVAR

cat concatenated_lgs.fa.partitions | \
perl -pe 's/.*lg/DNA, lg/g; s/\.fa//g; s/\n/;\n/g;' \
> concatenated_lgs.partitions

cat concatenated_lgs.varsites.fa.partitions | \
perl -pe 's/.*lg/DNA, lg/g; s/\.fa\.varsites\.fa//g; s/\n/;\n/g;' \
> concatenated_lgs.varsites.partitions

rm concatenated_lgs.fa.partitions
rm concatenated_lgs.varsites.fa.partitions
# .............................................................................


# run modelfinder using BIONJ starting tree
# (and use substitution model and partitioning for all hypothesis testing 
# afterwards)
# .............................................................................
mkdir modelfinder

# asc
$IQTREE \
-nt $NTHREADS \
-st DNA \
-m MF+ASC+MERGE -msub nuclear \
-s concatenated_lgs.varsites.fa \
-sp concatenated_lgs.varsites.partitions \
-t BIONJ \
-pre modelfinder/concatenated_lgs.varsites \
-quiet

# normal
$IQTREE \
-nt $NTHREADS \
-st DNA \
-m MF+MERGE -msub nuclear \
-s concatenated_lgs.fa \
-sp concatenated_lgs.partitions \
-t BIONJ \
-pre modelfinder/concatenated_lgs \
-quiet

# wait for processes to finish
wait

# fix nexus if need be
# asc
NSC=$(grep "charset" modelfinder/concatenated_lgs.varsites.best_scheme.nex | grep -o ";" | wc -l)
if [ $NSC -gt 1 ];
then
	perl -pi -e 's/\= ([0-9]+).*\-/\= $1-/g; s/\;+$/\;/g' modelfinder/concatenated_lgs.varsites.best_scheme.nex
fi

# normal
NSC=$(grep "charset" modelfinder/concatenated_lgs.best_scheme.nex | grep -o ";" | wc -l)
if [ $NSC -gt 1 ];
then
	perl -pi -e 's/\= ([0-9]+).*\-/\= $1-/g; s/\;+$/\;/g' modelfinder/concatenated_lgs.best_scheme.nex
fi
# .............................................................................

# split phylogenetic hypotheses and fit ML using substitution model 
# and partitioning
# .............................................................................
NPHYLOHYPOS=$(cat $PHYLOHYPOS | wc -l)
mkdir phylohypos
split -a 1 --numeric-suffixes=1 -l 1 $PHYLOHYPOS phylohypos/phylohypo

echo -n > phylohypos.varsites.ml.trees
echo -n > phylohypos.ml.trees

for ph in $(ls phylohypos/phylohypo[0-9]);
do
	echo "PHYLOHYPO $ph"

	# fit ML using each phylohypo
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# asc
	$IQTREE \
	-nt $NTHREADS \
	-st DNA \
	-s concatenated_lgs.varsites.fa \
	-spp modelfinder/concatenated_lgs.varsites.best_scheme.nex \
	-g $ph \
	-pre $ph.varsites \
	-wsl \
	-quiet

	# normal
	$IQTREE \
	-nt $NTHREADS \
	-st DNA \
	-s concatenated_lgs.fa \
	-spp modelfinder/concatenated_lgs.best_scheme.nex \
	-g $ph \
	-pre $ph \
	-wsl \
	-quiet

	wait
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++

	# add to trees to file to be used for tests with iqtree
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# asc
	cat $ph.varsites.treefile >> phylohypos.varsites.ml.trees
	# normal
	cat $ph.treefile >> phylohypos.ml.trees
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++

	# add to sitelh file to be used for tests with consel
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++
	# asc
	if [ ! -f phylohypos_varsites.sitelh ];
	then
		echo -n "$NPHYLOHYPOS " > phylohypos_varsites.sitelh
		head -n1 $ph.varsites.sitelh | cut -d" " -f2 >> phylohypos_varsites.sitelh 
	fi
	tail -n1 $ph.varsites.sitelh | perl -pe 's/Site_Lh/'$(basename $ph)'/g' >> phylohypos_varsites.sitelh

	# normal
	if [ ! -f phylohypos.sitelh ];
	then
		echo -n "$NPHYLOHYPOS " > phylohypos.sitelh
		head -n1 $ph.sitelh | cut -d" " -f2 >> phylohypos.sitelh 
	fi
	tail -n1 $ph.sitelh | perl -pe 's/Site_Lh/'$(basename $ph)'/g'>> phylohypos.sitelh
	# +++++++++++++++++++++++++++++++++++++++++++++++++++++++
done
# .............................................................................

# AU test with IQTREE using substitution and partitioning model 
# .............................................................................
# asc
$IQTREE \
-nt $NTHREADS \
-st DNA \
-spp modelfinder/concatenated_lgs.varsites.best_scheme.nex \
-s concatenated_lgs.varsites.fa \
-z phylohypos.varsites.ml.trees \
-pre iqtree_AU_test_varsites \
-zb 100000 \
-zw \
-au \
-wsl \
-quiet

# normal
$IQTREE \
-nt $NTHREADS \
-st DNA \
-spp modelfinder/concatenated_lgs.best_scheme.nex \
-s concatenated_lgs.fa \
-z phylohypos.ml.trees \
-pre iqtree_AU_test \
-zb 100000 \
-zw \
-au \
-wsl \
-quiet

wait
# .............................................................................

# AU tests with consel (maximum likelihood estimates and weighted least squares)
# .............................................................................

# asc
echo "consel using variable sites (ASC) with iqtree sitelh"
echo "--------------------------------------------------------------------------"
makermt --puzzle iqtree_AU_test_varsites.sitelh consel_iqtree_AU_test_varsites.rmt
consel --mle consel_iqtree_AU_test_varsites.rmt consel_iqtree_AU_test_varsites_mle
catpv -e -v consel_iqtree_AU_test_varsites_mle > consel_iqtree_AU_test_varsites_mle.dsv
catci consel_iqtree_AU_test_varsites_mle > consel_iqtree_AU_test_varsites_mle_ci.dsv
consel --wls consel_iqtree_AU_test_varsites.rmt consel_iqtree_AU_test_varsites_wls
catpv -e -v consel_iqtree_AU_test_varsites_wls > consel_iqtree_AU_test_varsites_wls.dsv
catci consel_iqtree_AU_test_varsites_wls > consel_iqtree_AU_test_varsites_wls_ci.dsv
echo "--------------------------------------------------------------------------"

# normal
echo "consel using all sites with iqtree sitelh"
echo "--------------------------------------------------------------------------"
makermt --puzzle iqtree_AU_test.sitelh consel_iqtree_AU_test.rmt
consel --mle consel_iqtree_AU_test.rmt consel_iqtree_AU_test_mle
catpv -e -v consel_iqtree_AU_test_mle > consel_iqtree_AU_test_mle.dsv
catci consel_iqtree_AU_test_mle > consel_iqtree_AU_test_mle_ci.dsv
consel --wls consel_iqtree_AU_test.rmt consel_iqtree_AU_test_wls
catpv -e -v consel_iqtree_AU_test_wls > consel_iqtree_AU_test_wls.dsv
catci consel_iqtree_AU_test_wls > consel_iqtree_AU_test_wls_ci.dsv
echo "--------------------------------------------------------------------------"


# asc
echo "consel using variable sites (ASC)"
echo "--------------------------------------------------------------------------"
makermt --puzzle phylohypos_varsites.sitelh consel_phylohypos_varsites.rmt
consel --mle consel_phylohypos_varsites.rmt consel_phylohypos_varsites_mle
catpv -e -v consel_phylohypos_varsites_mle > consel_AU_test_varsites_mle.dsv
catci consel_phylohypos_varsites_mle > consel_AU_test_varsites_mle_ci.dsv
consel --wls consel_phylohypos_varsites.rmt consel_phylohypos_varsites_wls
catpv -e -v consel_phylohypos_varsites_mle > consel_AU_test_varsites_wls.dsv
catci consel_phylohypos_varsites_mle > consel_AU_test_varsites_wls_ci.dsv
echo "--------------------------------------------------------------------------"

# normal
echo "consel using all sites"
echo "--------------------------------------------------------------------------"
makermt --puzzle phylohypos.sitelh consel_phylohypos.rmt
consel --mle consel_phylohypos.rmt consel_phylohypos_mle
catpv -e -v consel_phylohypos_mle > consel_AU_test_mle.dsv
catci consel_phylohypos_mle > consel_AU_test_mle_ci.dsv
consel --wls consel_phylohypos.rmt consel_phylohypos_wls
catpv -e -v consel_phylohypos_mle > consel_AU_test_wls.dsv
catci consel_phylohypos_mle > consel_AU_test_wls_ci.dsv
echo "--------------------------------------------------------------------------"
# .............................................................................

