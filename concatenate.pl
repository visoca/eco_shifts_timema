#!/usr/bin/perl

# Copyright (C) 2016 Victor Soria-Carrasco
# victor.soria.carrasco@gmail.com
# Last modified: 02/08/2016 03:02:29

# Description:
# Concatenate a list of fasta files by a particular field in sequence names
# See arguments below.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Note:
# ToDo:

# Changelog:
#	1.1-2016.06.12 - added options for arguments
#	1.1.1-2016.08.02 - fixed small bug when generating paritions file

use strict;
use warnings;
use Encode;
use Encode::Guess;
use File::Basename;
use File::Spec qw(rel2abs);
use Getopt::Long;
use Text::Wrap;
$Text::Wrap::columns=80;

my $version='1.1.1-2016.08.02';

&author;

my $mischar='-'; # Character for missing sequences
my $locisep=('?' x 10); # separator between loci # debug

my @ifiles=();
my $ofile='';
my $field=0; # field identifier
my $sepchar='-'; # character separating fields
my $partitions=0;
my @errors=();

GetOptions(
    'i|I=s'       => \@ifiles,
    'o|O=s'       => \$ofile,
	'f|F=i'       => \$field,
	's|S=s'       => \$sepchar,
	'p|P'         => \$partitions,
	'h|help'      => \&usage
);

&usage if	(!defined($ifiles[0]) ||
			(!defined($ofile)));

$ofile=File::Spec->rel2abs($ofile);


my @fastas=();
my @fastas_len=();
my %ids; # unique ids
foreach my $ifile (@ifiles){
	print "Processing file $ifile\n";
	$ifile=File::Spec->rel2abs($ifile);
	if ( -e $ifile){
		my ($f,$l,$e)=read_fasta($ifile,$sepchar,$field);
		my %fasta=%$f;
		my $alilen=$$l;
		push (@errors, @$e);
		foreach my $id (keys %fasta){
			$ids{$id}=1;
		}
		push (@fastas, \%fasta);
		push (@fastas_len, $alilen);
	}
	else{
		push (@errors, "File $ifile does not exist");
	}
}

die ("\n\nErrors found:\n\t".join ("\n\t",@errors)."\n") 
	if (defined($errors[0]));


# Concatenate by id
print "\nConcatenating by field #".$field."\n";
# print "\t".join ("\n\t", keys %ids)."\n"; # debug
my %concatenated;
foreach my $j (0..$#fastas){
	foreach my $i (keys %ids){
		if (defined($fastas[$j]{$i})){
			$concatenated{$i}.=$fastas[$j]{$i};
			# $concatenated{$i}.=$locisep; # debug
		}
		else{
			$concatenated{$i}.=($mischar x $fastas_len[$j]);
			# $concatenated{$i}.=$locisep; # debug
		}
	}
}

# output
open (FILE, ">$ofile")
	or die ("\n\nCan't write to file $ofile\n\n");
foreach my $id (sort keys %concatenated){
	print FILE ">$id\n";
	print FILE wrap('','', $concatenated{$id}."\n");
	print FILE "\n";
}
close (FILE);
print "\nConcatenated alignment saved as $ofile\n";

if ($partitions==1){
	open (FILE, ">$ofile.partitions")
		or die ("\n\nCan't write to file $ofile.partitions\n\n");
	my $len=0; 
	foreach my $j (0..$#ifiles){
		print FILE basename($ifiles[$j])." = ".($len+1)."-".($fastas_len[$j]+$len)."\n";
		# $len+=$fastas_len[$j]+1;
		$len+=$fastas_len[$j];
	}
	close (FILE);
	print "\nPartitions file saved as $ofile.partitions\n";
}
print "\n";


# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================


# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version     \n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input files>\n";
	print "      -o <output file>\n";
	print "      -s <field separator character (optional,default='-')>\n";
	print "      -f <field to use as id (optional,default=0)>\n";
	print "      -p <output partitions text (optional,default)>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================

# Read fasta
# ==============================================================================
sub read_fasta{
	my $ifile=shift;
	my $sepchar=shift;
	my $field=shift;

	open (FILE, "$ifile")
		or die ("\n\nCan't open file $ifile\n\n");
		my $id='';
		my %fasta=();
		while (<FILE>){
			my $enc = guess_encoding($_, qw/UTF-8 UTF-16LE ISO-8859-1 MacRoman/); # Detect encoding
			$_=$enc->decode($_); # Decode using encoding detected
			s/\r|\r\n/\n/g; # handle dos and mac endlines
			chomp;
			if (/^\>/){
				s/^\>//g;
				my @aux=split(/$sepchar/,$_);
				if (defined($aux[$field])){
					$id=$aux[$field];
				}
				else{
					push (@errors, "Field #$field not available in file $ifile (there are only ".scalar(@aux)." fields)");
					last;
				}
			}
			else{
				$fasta{$id}.=$_;
			}
		}
	close (FILE);

	# check this is an alignment
	my $len=0;
	my @errors=();
	foreach my $id (keys %fasta){
		if ($len!=0 && length($fasta{$id}) != $len){
			print "ID $id - LEN: $len - ".length($fasta{$id})."\n";
			push (@errors, "File $ifile is not an alignment");
			last;
		}
		# print "this: ".length($fasta{$id})."\n";
		$len=length($fasta{$id})
	}

	return (\%fasta, \$len, \@errors);
}
# ==============================================================================


sub recode_UTF2ISO{
	my @lines=@_;
	foreach my $l (@lines){
		my $text=decode("UTF-16", $l);
		$l=encode("ISO-8859-1", $text);
	}
	return(@lines);
}


