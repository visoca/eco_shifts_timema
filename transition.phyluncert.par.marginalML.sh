#!/bin/bash
#$ -l h_rt=07:00:00
#$ -j y
#$ -o transition.phyluncert.par.marginalML.log
#$ -cwd
#$ -r y
#$ -t 1-1001
#$ -V

### iceberg
##$ -pe openmp 1
##$ -l arch=intel*
##$ -l arch=intel-e5*

### aws and sharc
##$ -pe smp 1

### popgenom
##$ -pe openmp 1
##$ -P popgenom
##$ -q popgenom.q
##$ -tc 40
#####

#$ -l h=!node042

# This script is a wrapper to run transition rate analysis in parallel using 
# transition.phyluncert.par.R in the HPC computer clusters of the University 
# of Sheffield

NTHREADS=1

# genomics repository
source /usr/local/extras/Genomics/.bashrc 

RSCRIPT="./transition.phyluncert.par.R"
TREEDIR="trees"
OUTDIR="res_marginalML"
LOGDIR="logs_marginalML"
METHOD="marginal" # "joint" "marginal" "scaled"
RECONS="subsequently" # "estimate" "subsequently"

# change to METHOD="marginal" and RECONS="subsequently" for marginal lhl inferences


TREES=($(find $TREEDIR -name "*.tree" | sort -V))

I=$((SGE_TASK_ID-1))

TREE=${TREES[$I]}

mkdir $LOGDIR >& /dev/null

LOG=$LOGDIR/$(basename $TREE | perl -pe 's/\.tree/\.log/g')

START=$(date +%s)

date > $LOG
hostname >> $LOG
cat /proc/cpuinfo | grep -m1 "model name" | perl -pe 's/.*\: //g' >> $LOG
echo "==========================================================" >> $LOG

cat >> $LOG << EOF
$RSCRIPT \
--tree $TREE \
--outdir $OUTDIR \
--method $METHOD \
--recons $RECONS \
--nthread $NTHREADS
==========================================================
EOF

$RSCRIPT \
--tree $TREE \
--outdir $OUTDIR \
--method $METHOD \
--recons $RECONS \
--nthread $NTHREADS \
>> $LOG 2>&1

echo "==========================================================" >> $LOG
date >> $LOG
echo >> $LOG

END=$(date +%s)
RUNTIME=$(($END-$START))

((sec=RUNTIME%60, RUNTIME/=60, min=RUNTIME%60, RUNTIME/=60, hrs=RUNTIME%24, days=RUNTIME/24))
printf "Run took: %dd %02dh %02dm %02ds\n" $days $hrs $min $sec >> $LOG

